
const routes = [
  {
    path: '/',
    component: () => import('layouts/IndexLayout.vue'),
    children: [
      { path: '', name: "index", component: () => import('pages/Index.vue') },

    ]
  },
  {
    path: "/analysis",
    component: () => import('layouts/MainLayout.vue'),
    children: [{
      path: '/analysis', name: "analysis", component: () => import('pages/Analysis.vue')
    }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
