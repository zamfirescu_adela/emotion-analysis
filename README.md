# Emotion App (emotion-analysis-app)

A Quasar Framework app

Analiza sentimentelor de baza, in special pentru utilizarea comerciala, poate fi redusa la clasificarea frazelor, alineatelor si a postarilor sau a documentelor ca fiind negative, neutre sau pozitive. O procesare mai complexa a sentimentului si atitudinii, extragerea sensului, clasificarea intentiei si analiza emotiilor bazate pe lingvistica castiga, de asemenea, teren. Se estimeaza ca 80% din datele lumii sunt nestructurate, neorganizate. Volume uriase de date text (e-mailuri, bilete de asistenta, chat-uri, conversatii pe retelele de socializare, sondaje, articole, documente etc.), sunt create in fiecare zi, dar sunt greu de analizat, de inteles si de rezolvat, lasand la o parte timpul si costul.

## Descriere problema

Analiza emotiilor are aplicatii in mai multe domenii, unii dintre cei mai interesati de ea fiind cei care activeaza in domeniul marketing-ului, deoarece continutul creat de ei se poate supune la un feedback automat si cat mai apropiat de realitate, fara sa mai fie nevoie de analize ce dureaza mult timp si care pot fi dificil de realizat. Insa in aceasta aplicatie am ales sa pun in aplicare o alta latura pe care poate fi folosita aceasta analiza, anume cea a vizualizarii emotiilor dupa analiza unor descrieri ale articolelor de stiri. 

## Descriere API

Utilizatorii vor avea la dispozitie un panou in care pot sa aleaga anumiti parametri dupa care se vor afisa stirile, cu ajutorul [NewsAPI.org](https://newsapi.org/docs/endpoints). Dintre cele doua endpoint-uri disponibile, l-am ales pe cel de `/v2/top-headlines`, pentru a afisa cele mai recente stiri; parametrii furnizati de utilizator se refera la **country** (tara, existand mai multe optiuni), **category** (categoria de interes, de exemplu `business` `environment` `general` `health` `science` si altele) si **pageSize** (numarul de articole pe care un utilizator ar putea fi interesat de afisat, 20 de articole implicit, maxim 100).  Am ales acest API deoarece are mai putine endpoint-uri, cu date agregate din mai multe surse stiri, care poate sa tina utilizatorii la curent cu stiri in timp real. 

Analiza emotiilor a fost realizata cu ajutorul API-ului oferit de la cei de la [ParallelDots](http://apis.paralleldots.com/text_docs/index.html), care dispune de mai multe endpoint-uri, putand fi facute analize complexe asupra sectiunilor de text furnizate; alaturi de analiza sentimentelor se afla si analiza emotiilor, folosind endpoint-ul `/v5/emotion_batch` pentru a transmite descrierile obtinute de pe urma NewsAPI pentru analiza. Spre deosebire de analiza de sentiment, care face o "scanare" generala a unui text si poate returna doar una din trei variante (fericire, suparare, neutralitate), analiza emotiilor ia in calcul si nuantele cuvintelor din propozitii, putand descrie mai concret ce fel de emotie este transmisa printr-un articol, anume suparare, fericire, frica, furie, entuziasm, neutralitate; pentru fiecare dintre acestea, se adauga un procent, insemnand cat de predominanta este o anumita emotie.  La fel ca si API-ul descris mai sus si acesta accepta mai multe limbi, insa pentru acest proiect am ales engleza ca si limba de baza. 

***Tehnologii folosite:***

***Quasar*** este un framework open-source, dezvoltat de catre un roman, peste Vue, popularul framework folosit pentru dezvoltarea de aplicatii SPA. De-a lungul timpului, Quasar s-a dezvoltat destul de mult, plecand de la componente stilizate de Vue, astazi ajungand la multe functionalitati built-in si suport pentru SSR, PWA, aplicatii Desktop, aplicatii Mobile Hibride. ***VueJS*** ajuta programatorii sa construiasca aplicatii SPA intr-un timp relativ scurt si fara un timp de invatare prea indelungat, fiind potrivit pentru cei care dezvolta aplicatii in timpul liber, demo si, in general, pentru cei care doresc sa foloseasca un framework lightweight. Acesta combina ce este mai bun din cei doi jucatori mari, React si Angular, impreuna cu plug-in-urile proprii, pentru a aduce programatorilor o solutie completa pentru problemele pe care vor sa le rezolve. 

***NodeJs*** si ***Express*** sunt o combinatie populara pentru a dezvolta servere rapide, de pe care aplicatiile pot creste, facand adaugarea endpoint-urilor rapida si fara sa incarce codul, conectand cu o sursa de date si oferind un mediu de dezvoltare a serverelor.  

***Heroku*** este o platforma de tip PAAS,  ce le permite dezvoltatorilor sa investeasca in aplicatii si mai putin in partea de operations. Heroku se ocupa de patch-uri si actualizari, securitate si sisteme dedicate construirii, in cazul esecurilor. Pentru fiecare aplicatie se creeaza o masina virtuala, care se reface de la 0 atunci cand se creeaza un nou build. Aceasta ofera statistici, log-uri cu erori si multe alte plugin-uri, totul pentru a face mai usoara publicarea aplicatiilor programatorilor. 

## Flux de date



1. **Exemple de request/response**

   

   ###### NewsAPI

   endpoint: ***https://newsapi.org/v2/top-headlines***

   method: ***GET***

   parameters: 

   - ***country*** (optional): stirile dintr-o anumita tara

   - ***category*** (optional): stirile dintr-o anumita categorie

   - ***pageSize*** (optional): numarul de stiri de pe o pagina

   - ***apiKey*** (necesar): apiKey-ul furnizat 

     ![na_r_1](https://gitlab.com/zamfirescu_adela/emotion-analysis/-/blob/master/img/na_r_1.PNG)

     

   ###### ParallelDotsAPI

   endpoint: ***https://apis.paralleldots.com/v5/emotion***

   method: ***POST***

   parameters: 

   - ***text***: textul supus analizei - string (pentru un singur text), Array (pentru mai multe texte)

   - ***api_key***: api_key-ul furnizat

     

   ![pd_r_1](https://gitlab.com/zamfirescu_adela/emotion-analysis/-/blob/master/img/pd_r_1.PNG)

   <sup>*toate datele sunt transmise in Body-ul request-ului, sub forma de form-data</sup>

   

2. **Metode HTTP**

   Metodele HTTP folosite au fost, pe rand, pentru API-ul de stiri, GET, pentru a lua informatiile oferite de serverul lor, prin endpoint, iar pentru cel de analiza POST, pentru a transmite catre server acele descrieri pentru a fi supuse la analiza. 

3. **Authentificare si servicii utilizate** 

   Ambele API-uri folosite necesita autentificarea cu un API key furnizat de aceste servicii, care se pot obtine dupa ce utilizatorii isi creeaza un cont. In cazul API-ului de la ParallelDots, exista o limita a request-urilor care se realizeaza in fiecare zi, maxim 1000, timp de 30 de zile, perioada in care este gratuita utilizarea acestuia. In cazul celor de la NewsAPI.org, limita se afla la 500 de request-uri pe zi si noi articole apar dupa 15 minute. Pentru aceasta, am ales planurile free de la fiecare dintre API-urile prezentate mai sus. 



## Capturi de ecran aplicatie

![as_1](https://gitlab.com/zamfirescu_adela/emotion-analysis/-/blob/master/img/as_1.PNG)

![as_2](https://gitlab.com/zamfirescu_adela/emotion-analysis/-/blob/master/img/as_2.PNG)

![as_3](https://gitlab.com/zamfirescu_adela/emotion-analysis/-/blob/master/img/as_3.PNG)



## Referinte

***API***: 

https://newsapi.org/docs/get-started

https://www.paralleldots.com/docs

***Front-End***: 

https://vuejs.org/

https://quasar.dev/start/pick-quasar-flavour

***Back-End***: 

https://nodejs.org/en/

https://expressjs.com/

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
